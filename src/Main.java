import java.io.BufferedReader;
import java.io.FileReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {

        int coreCount = Runtime.getRuntime().availableProcessors();

        FileReader fileReader;
        BufferedReader bufferedReader;
        try {
            fileReader = new FileReader("data.txt");
            bufferedReader = new BufferedReader(fileReader);
            //ExecutorService executorService = Executors.newWorkStealingPool(coreCount);
            ExecutorService executorService = Executors.newFixedThreadPool(coreCount);
            String currLine;
            while ((currLine = bufferedReader.readLine()) != null) {
                String trimLine = currLine.trim();
                executorService.submit(new Task(trimLine));
            }

            bufferedReader.close();
            fileReader.close();

            executorService.shutdown();
            try {
                if (!executorService.awaitTermination(800, TimeUnit.MILLISECONDS)) {
                    executorService.shutdownNow();
                }
            } catch (InterruptedException e) {
                executorService.shutdownNow();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class Task implements Runnable {
    String line;

    public Task(String line) {
        this.line = line;
    }

    @Override
    public void run() {
        System.out.println("[" + Thread.currentThread().getName() + "] " + "Message " + this.line);
    }
}

